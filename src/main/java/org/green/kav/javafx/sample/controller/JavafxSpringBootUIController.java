package org.green.kav.javafx.sample.controller;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class JavafxSpringBootUIController {
    private static final Logger logger = LoggerFactory.getLogger(JavafxSpringBootUIController.class);
    @FXML
    TextField investigationName;

    @FXML
    TextField fromDir;

    @FXML
    TextField toDir;

    @FXML
    public void submit(Event e){
        logger.info("investigationName: ", investigationName.getText());
        logger.info("fromDir: ", fromDir.getText());
        logger.info("toDir: ", toDir.getText());
    }
}
