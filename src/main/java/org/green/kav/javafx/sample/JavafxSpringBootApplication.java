package org.green.kav.javafx.sample;

import javafx.application.Application;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavafxSpringBootApplication {

	public static void main(String[] args) {
		Application.launch(JavafxSpringBootUIApplication.class, args);
	}

}
